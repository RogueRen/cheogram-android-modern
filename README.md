# Cheogram Android

This is a fork of [Cheogram](https://git.singpolyma.net/cheogram-android) to have a more modern-looking design, including:

* Rounded message bubbles
* Circular profile images
* Improved Dark/Light theme colors
* New color scheme for the OLED theme

The Cheogram Android app allows you to join a worldwide communication network.  It especially focuses on features useful to users who want to contact those on other networks as well, such as SMS-enabled phone numbers.

Based on the app Conversations, but with unique features:

* Messages with both media and text, including animated media
* Unobtrusive display of subject lines, where present
* Links to known contacts are shown with their name
* Integrates with gateways' add contact flows
* When using a gateway to the phone network, integrate with the native Android Phone app
* Address book integration

<img src="media/preview-white.png" alt="White theme" width="200"/>
<img src="media/preview-dark.png" alt="Dark theme" width="200"/>
<img src="media/preview-dark-obsidian.png" alt="Obsidian theme" width="200"/>
<img src="media/preview-dark-amoled.png" alt="OLED theme" width="200"/>
